# OwnDevices

![icon](https://gitlab.com/vriska/owndevices/-/raw/main/src/main/resources/assets/owndevices/icon.png)

OwnDevices is a mod intended to leave OpenAL Soft to its own device selection.
By default, recent versions of Minecraft (since 1.18 or so) will check the default output device at startup,
force that device, and reinitialize the entire sound system if it becomes unavailable.

I assume that this was intended to work around some issue with OpenAL Soft.
However, this causes various issues of its own, most prominently music stopping when the default output changes
(or sometimes even audio breaking entirely).
The issues it was intended to work around may no longer even exist in the latest OpenAL Soft releases.

This mod contains a single mixin that makes detecting the default device always fail.
Minecraft already handles this scenario, and will fall back to letting OpenAL Soft pick the device.
It will likely work unchanged on all Minecraft versions with the relevant behavior,
though it's only been tested on 1.20.1.
