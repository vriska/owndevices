package dev.vriska.owndevices.mixin;

import net.minecraft.client.sound.SoundEngine;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(SoundEngine.class)
public class SoundSystemMixin {
	@Inject(method = "findAvailableDeviceSpecifier", at = @At("HEAD"), cancellable = true)
	private static void findAvailableDeviceSpecifier(CallbackInfoReturnable<String> ci) {
		ci.setReturnValue(null);
	}
}
